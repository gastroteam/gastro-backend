.PHONY: dist push

VERSION := 1.0.0

build:
	docker compose build
run:
	make build
	docker compose up -d
start:
	docker compose up -d
stop:
	docker compose down

debug_up:
	docker compose -f docker-compose-debug.yml up -d

debug_down:
	docker compose -f docker-compose-debug.yml down

dist:
	docker build -t "gastroteam:${VERSION}" ./
	docker tag "gastroteam:${VERSION}" "akruglov/gastroteam:${VERSION}"
	docker tag "gastroteam:${VERSION}" "akruglov/gastroteam:latest"

push: dist
	docker login
	docker image push "akruglov/gastroteam:${VERSION}"
	docker image push "akruglov/gastroteam:latest"